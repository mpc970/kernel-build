#!/bin/env python

from os import path
from os import cpu_count
import subprocess
import sys


def check_config():
    print ("check for .config and make oldconfig")
    if not path.exists('.config') or not path.exists('Kconfig'):
        print ("Not in a kernel Directory! or no .config found")
        sys.exit()

def kernel_build():
    print ("here is the build aka make")
    subprocess.run(f"make -j{cpu_count()}", shell=True, check=True)

def kernel_install():
    print ("installs kernel")
    subprocess.run("sudo make install", shell=True, check=True)

    print ("installs kernel modules")
    subprocess.run("sudo make modules_install", shell=True, check=True)

    print ("installs initramfs")
    subprocess.run("sudo mkinitcpio -p linux-custom", shell=True, check=True)

def json_compile_database():
    print ("Building compile_commands.json")
    subprocess.run("make compile_commands.json", shell=True, check=True)

check_config()
kernel_build()
kernel_install()
json_compile_database()

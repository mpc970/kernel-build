#!/bin/bash
# kernel build script, Ryan Hanlon 2018-2021

set -C -e

if [[ ! -e "Kconfig" ]]; then
	echo "Not in Kernel Directory! quitting."
	exit 0
fi

if [[ ! -e ".config" ]]; then
	echo "No .config file found. :("
	exit 0
fi

function kernel_build () {
echo "Building kernel!"

ThreadCount=$(grep -c ^processor /proc/cpuinfo)
Time=$(date '+%Y-%m-%d_%H%M')
echo "Building with $ThreadCount threads"

make V=1 -j${ThreadCount} 2>&1 | tee ~/kernels/build-output/${Time}-make.log | awk '/^  gcc/ {print $1 " " $2}'

}

function kernel_install () { # Function to install the kernel, modules and initrd
echo "Sudo Required to install , Enter password"

echo "Installing kernel..."
sudo make install

echo "Installling Modules"
sudo make modules_install

echo "Building initramfs"

sudo mkinitcpio -p linux-custom

}

function json_compile_database () { # Builds compile_commands for langage servers

echo "Building compile_commands.json"
make compile_commands.json

}

kernel_build
kernel_install 2>&1 | tee -a ~/kernels/build-output/${Time}-make.log 2>&1
json_compile_database
